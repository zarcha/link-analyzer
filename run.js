var fs = require("fs");
const { resolve } = require("path");
var bmp = require("bmp-js");
var Jimp = require("jimp")
let newHex = "";
let command = process.argv[2]
var args = process.argv.slice(3);
let columns = ["0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F"]

switch(command){
    case "getDiff":
        console.log(args)
        getDiff(args[0], args[1])
        break;
    case "getNewCheckBit":
        getNewCheckBit(args[0], args[1], args[2]);
        break;
    case "convert":
        convertFileToHex(args[0])
        break;
    case "flip":
        flipFileEndianess(args[0])
        break;
    case "convertFromBMP":
        convertBMPtoHex(args[0])
        break;
    case "convertToBMP":
        convertHextoBMP(args[0], args[1], args[2])
        break;
    case "getSpan":
        getSpan(args[0], args[1])
        break;
    default:
        console.log("Command does not exist")
}

// console.log(hex2int("64"))
// console.log(getHexDifferences("85", "A8"))
// console.log(int2hex(hex2int("2E") + (getHexDifferences("B1", "92"))));

function getSpan(start, end){
    start = hex2int(start);
    end = hex2int(end);

    for(let i = start; i <= end; i++){
        process.stdout.write(`"${int2hex(i)}",`)
    }
}

function convertBMPtoHex(file){
    var bmpBuffer = fs.readFileSync(file);
    console.log(bmp.decode(bmpBuffer))
    var bmpData = bmp.decode(bmpBuffer).data.toString('HEX');
    let binResult = "";
    let endResult = "";

    for(let i = 0; i < bmpData.length; i+=8){
        if(bmpData.substr(i, 8) == "00ffffff"){
            binResult += "0"
        }else{
            binResult += "1"
        }
    }

    for(let i = 32; i < binResult.length-32; i+=8){
        endResult += bin2hex(flipEndainess(binResult.substr(i, 8)));
        
    }
    
    fs.writeFile("./converted.Bin", endResult, 'HEX', (err) => {
        if(err){
            console.log(err);
            return;
        }

        console.log("File written")
    })
}

function convertHextoBMP(file){
    let height = 32,
        width = height;

    fs.readFile(file, 'HEX', (err, data) => {
        if(err){
            console.log(err);
            return;
        }

        let lines = []
        lines.push("000000000000000000000000000000000")
        for(let j = 0; j < data.length; j+=8){
            let set = data.substr(j, 8);
            let binConverted = "";
            for(let i = 0; i < set.length; i+=2){
                binConverted += flipEndainess(hex2bin(set.substr(i, 2)))
            }

            lines.push(binConverted);
        }
        lines.push("000000000000000000000000000000000")

        let jimg = new Jimp(32, 32);
        for (var y=0; y<height; y++) {
            for(let x = 0; x < width; x++){
                let color = lines[y].substr(x, 1) == "0" ? "FFFFFFFF" : "000000FF"
                jimg.setPixelColor(parseInt(color, 16), x, y);
            }
        }
        jimg.write("test2.bmp")
    });
}

function getNewCheckBit(check, oldValue, newValue){
    let checkVal = hex2int(check) + (getHexDifferences(oldValue, newValue));
    console.log(checkVal)
    if(checkVal < 0) checkVal = 256 + checkVal;
    if(checkVal < 255) checkVal = 0 + checkVal;
    console.log(checkVal)
    console.log(int2hex(checkVal));
}

function getHexDifferences(hex1, hex2){
    return hex2int(hex1) - hex2int(hex2);
}

async function getDiff(file1, file2){
    let data1 = await getFileData(file1);
    let data2 = await getFileData(file2);

    let row = 0,
        column = 0,
        size = 2,
        length = (data1.length > data2.length ? data1.length : data2.length);

    for(let i = 0; i < length; i+=size){
        if(column > 15) {
            column = 0;
            row++;
        }
        let value1 = data1.substr(i, size);
        let value2 = data2.substr(i, size)

        if(value1 != value2) console.log("0x" + row + columns[column] + " | Value1 :" + value1 + " | Value2: " + value2);
        
        column++;
    }
}

async function convertFileToHex(file){
    let data = await getFileData(file);
    let newData = "";

    for(let i = 0; i < data.length; i+=2){
        console.log(int2hex(parseInt(data.substr(i, 2), 16)))
        newData += int2hex(parseInt(data.substr(i, 2), 16));
    }

    fs.writeFile("./test2.Bin", newData, 'HEX', (err) => {
        if(err){
            console.log(err);
            return;
        }

        console.log("File written")
    })
}


async function getFileData(file){
    return new Promise((resolve, reject) => {
        fs.readFile(file, 'utf8', (err, data) => {
            if(err){
                reject(err);
                return;
            }
    
            resolve(data);
        });
    });
}

async function flipFileEndianess(data){
    fs.readFile(data, 'HEX', (err, data) => {
        if(err){
            console.log(err);
            return;
        }
    
        let size = 2;
        for(let i = 0; i < data.length; i = i + size){
            newHex += bin2hex(flipEndainess(hex2bin(data.substr(i, size))))
        }
    
        fs.writeFile("./converted.Bin", newHex, 'HEX', (err) => {
            if(err){
                console.log(err);
                return;
            }
    
            console.log("File written")
        })
    })
}


function flipEndainess(str){
    return str.split("").reverse().join("")
}

function hex2int(hex){
    return parseInt(hex, 16);
}

function int2hex(int){
    let size = 4;
    let space = "";
    for(i = 0; i < size; i++){
        space += "0"
    }
    return (space + (int).toString(16)).substr(-size)
}

function hex2bin(hex){
    return ("00000000" + (parseInt(hex, 16)).toString(2)).substr(-8)
}

function bin2hex(bin){
    return ("00" + (parseInt(bin, 2)).toString(16)).substr(-2)
}
const { ipcRenderer, remote } = require('electron')

export default function($scope) {

    //Transitions
    $scope.menuToShow = "searchingForCom"
    

    ipcRenderer.on('comFound', (event, arg) => {
        $scope.menuToShow = "detectChip";
        $scope.$apply();
    });

    ipcRenderer.send("searchForCom");

}


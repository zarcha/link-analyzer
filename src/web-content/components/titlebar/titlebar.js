const { ipcRenderer, remote } = require('electron')

export default {
    templateUrl: "./components/titlebar/titlebar.html",
    transclude: true,
    controllerAs: 'ctrl',
    controller: function(){
        let ctrl = this;

        ctrl.closeWindow = function(){
            ipcRenderer.send('closeApp');
        }
    
        ctrl.minimiseWindow = function(){
            let window = remote.getCurrentWindow();
            window.minimize(); 
        }
    }
}
const { ipcRenderer, remote } = require('electron');
const shortid = require("shortid");

export default function($scope, $location){
    let scope = $scope;
    scope.advancedEnabled = false;
    scope.disableWrite = false;

    scope.saveBackup = () => {
        ipcRenderer.send("saveBackup", scope.naviData);
    }

    scope.loadBackup = () => {
        ipcRenderer.send("loadBackup");
        scope.waiting = true;
        $scope.$apply();
    }

    scope.changeNavi = (name) => {
        scope.waiting = true;
        // $scope.$apply();
        name = name.split("/")[0];
        ipcRenderer.send("loadBackup", name);
    }

    scope.getOptionsValue = (data) => {
        if(data.value != null && data.value != undefined && data.type == "dropdown"){
            if(data.value in data.options){
                return data.options[data.value.toString()];
            }else{
                return data.options["-1"];
            }
        }

        return "";
    }

    scope.changeValues = (data) => {
        let start = parseInt(data.location[0], 16) * 2,
            end = (parseInt(data.location[data.location.length-1], 16) + 1) * 2;

        let valueToWrite = data.value;
        console.log(data.type)
        if(data.type != "hex" && data.type != "image") valueToWrite = convertValuesToHex(data.type, valueToWrite);
        if(data.type == "image") {
            data.fileName = `${data.field}-${shortid.generate()}`
            ipcRenderer.send("saveImage", {name: data.fileName, data: data.value});
        }

        for(let i = 0; i < valueToWrite.length; i++){
            scope.naviData = scope.naviData.replaceAt(start + i, valueToWrite[i]);
        }
    }

    scope.writeData = () => {
        if(confirm("This will overwrite the current data on your navi chip, are you sure you want to do this?")){
            ipcRenderer.send("writeNaviData", scope.naviData);
            scope.waiting = true;
        }
    }

    scope.getValue = (data) => {
        let start = parseInt(data.location[0], 16) * 2,
            end = (parseInt(data.location[data.location.length-1], 16) + 1) * 2,
            endData = "";

        for(let i = start; i < end; i+=2){
            endData += scope.naviData.substr(i, 2);
        }

        if(data.type != "hex" && data.type != "image") endData = convertValuesFromHex(data.type, endData);
        if(data.type == "image") {
            data.fileName = `${data.field}-${shortid.generate()}`
            ipcRenderer.send("saveImage", {name: data.fileName, data: endData});
        }

        return endData;
    }

    scope.changeImage = (name) => {
        ipcRenderer.send("loadImage", name);
    }

    function convertValuesFromHex(type, data){
        let endData;

        switch(type){
            case "dropdown":
            case "number":
                endData = hex2int(data);
                break;
            case "text":
                endData = "";
                for(let i = 0; i < data.length; i+=2){
                    endData += String.fromCharCode(96 + hex2int(data.substr(i, 2))).toUpperCase();
                }
                break;
            case "binary":
                endData = hex2bin(data);
                break;
            case "lendian":
                let multiplier = hex2int(data.substr(2))
                endData = ((255 * multiplier) + hex2int(data.substr(0, 2))) + multiplier;
                break;
        }

        return endData;
    }

    function convertValuesToHex(type, data){
        let endData;

        switch(type){
            case "dropdown":
            case "number":
                endData = int2hex(parseInt(data));
                break;
            case "text":
                endData = "";
                for(let i = 0; i < data.length; i++){
                    endData += int2hex((data.toLowerCase().charCodeAt(i) - 96))
                }
                break;
            case "binary":
                endData = bin2hex(data);
            case "lendian":
                let multiplier = Math.floor(data / 256);
                endData = int2hex(data - ((255 * multiplier) + multiplier)) + int2hex(multiplier);
                break;
        }

        return endData;
    }

    function hex2int(hex){
        return parseInt(hex, 16);
    }

    function int2hex(int){
        return ("00" + (int).toString(16)).substr(-2)
    }

    function hex2bin(hex){
        return ("00000000" + (parseInt(hex, 16)).toString(2)).substr(-8)
    }
    
    function bin2hex(bin){
        return ("00" + (parseInt(bin, 2)).toString(16)).substr(-2)
    }

    ipcRenderer.on('responseLocation', (event, arg) => {
        scope.appPath = arg;
    });

    ipcRenderer.on('responseNaviDataLocattions', (event, arg) => {
        scope.dataLoctions = arg;
    });

    ipcRenderer.on('responseNaviData', (event, arg) => {
        scope.naviData = arg;
        for(let i = 0; i < scope.dataLoctions.length; i++){
            scope.dataLoctions[i].value = scope.getValue(scope.dataLoctions[i])
        }
        setTimeout(()=> {
            scope.waiting = false;
            $scope.$apply();
        }, 2000)
        
    });

    ipcRenderer.on('imageChange', (event, arg) => {
        for(let i = 0; i < scope.dataLoctions.length; i++){
            if(scope.dataLoctions[i].field == arg.name){
                scope.dataLoctions[i].value = arg.data;
                scope.changeValues(scope.dataLoctions[i]);
                alert("Image replaced but not written to the Navi chip.");
                $scope.$apply();
                return;
            }
        }
    })

    ipcRenderer.on('responseCheckForFake', (event, arg) => {
        console.log(arg)
        scope.disableWrite = arg;
    });

    ipcRenderer.on('cancleWait', (event, arg) => {
        scope.waiting = false;
        $scope.$apply();
    });

    ipcRenderer.on('errorMessage', (event, arg) => {
        alert(arg);
    });

    scope.back = () => {
        ipcRenderer.removeAllListeners();
        $location.path("/detect");
        $scope.$apply();
    }

    String.prototype.replaceAt = function(index, replacement) {
        return this.substr(0, index) + replacement + this.substr(index + replacement.length);
    }

    scope.naviData = null;
    ipcRenderer.send("checkForFake");
    ipcRenderer.send('getNaviDataLocattions');
    ipcRenderer.send("getNaviData");
    ipcRenderer.send("getLocation");
    scope.waiting = true;
}

const { ipcRenderer, remote } = require('electron');

export default function($scope, $location){
    let scope = $scope;

    scope.chipFileData;

    scope.detectChip = () => {
        scope.waiting = true;
        ipcRenderer.send("detectChip");
    }

    ipcRenderer.on('chipPinout', (event, arg) => {
        let id = getIdValue(arg.trim())
        let index = getChipData(id);
        if(index != -1){
            ipcRenderer.removeAllListeners();
            let chip = scope.chipFileData[index];
            if(chip.class != "Navi Data Chip"){
                $location.path(`/standardChip/${arg}`);
                console.log("normal")
            }else{
                $location.path("/naviChip");
                console.log("navi chip")
            }
        }
        
        scope.waiting = false;
        $scope.$apply();
    });

    function getChipData(id){
        let index = scope.chipFileData.findIndex( 
            value => { return value.id == id } );
    
        return index;
    }

    function getIdValue(pinout){
        let bin = pinout.substr(2).slice(0, -1).split("").reverse().join("").padStart(16, '0')
        let indexId = parseInt(bin, 2) >> 0
        return indexId.toString().padStart(3, '0');
    }

    ipcRenderer.on('responseChipLibrary', (event, arg) => {
        scope.chipFileData = arg;
    });

    ipcRenderer.send('getChipLibrary');
    
}

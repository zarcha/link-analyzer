const { ipcRenderer, remote } = require('electron');

export default function($scope, $location, $routeParams){
    let scope = $scope;
        scope.pins = $routeParams.pins;

    ipcRenderer.on('responseLocation', (event, arg) => {
        scope.appPath = arg;
    });

    scope.back = () => {
        ipcRenderer.removeAllListeners();
        $location.path("/detect");
        $scope.$apply();
    }

    ipcRenderer.on('responseChipLibrary', (event, arg) => {
        scope.chipFileData = arg;
        let id = getIdValue($routeParams.pins)
        let index = getChipData(id);
        if(index != -1){
            scope.chipData = scope.chipFileData[index];
            console.log(scope.chipData)
        }

        scope.waiting = false;
        $scope.$apply();
    });

    function getChipData(id){
        let index = scope.chipFileData.findIndex( 
            value => { return value.id == id } );
    
        return index;
    }

    function getIdValue(pinout){
        let bin = pinout.substr(2).slice(0, -1).split("").reverse().join("").padStart(16, '0')
        let indexId = parseInt(bin, 2) >> 0
        return indexId.toString().padStart(3, '0');
    }

    ipcRenderer.send('getChipLibrary');
    ipcRenderer.send("getLocation");
}

const { ipcRenderer, remote } = require('electron');

export default function($scope, $location){
    let scope = $scope;

    scope.skipComCheck = () => {
        ipcRenderer.send("useFakeCom");
    }

    ipcRenderer.on('comFound', (event, arg) => {
        $location.path("/detect");
        $scope.$apply();
    });

    ipcRenderer.send("searchForCom");
}

import searchingCtl from "./pages/searching/searching.js";
import detectCtrl from "./pages/detect/detect.js"
import naviChipCtrl from "./pages/naviChip/naviChip.js"
import standardChipCtrl from "./pages/standardChip/standardChip.js"

import titlebar from "./components/titlebar/titlebar.js"
import loading from "./components/loading/loading.js"

let app = angular.module('app', ['ngRoute']);

app.controller("searchingCtl", ["$scope", "$location", searchingCtl]);
app.controller("detectCtrl", ["$scope", "$location", detectCtrl]);
app.controller("naviChipCtrl", ["$scope", "$location", naviChipCtrl]);
app.controller("standardChipCtrl", ["$scope", "$location", "$routeParams", standardChipCtrl]);

app.component('titlebar', titlebar);
app.component('loading', loading);

app.config(function($routeProvider) {
  $routeProvider
  .when("/searching", {
      templateUrl: "./pages/searching/searching.html",
      controller: "searchingCtl"
  })
  .when("/detect", {
    templateUrl: "./pages/detect/detect.html",
    controller: "detectCtrl"
  })
  .when("/naviChip", {
    templateUrl: "./pages/naviChip/naviChip.html",
    controller: "naviChipCtrl"
  })
  .when("/standardChip/:pins", {
    templateUrl: "./pages/standardChip/standardChip.html",
    controller: "standardChipCtrl"
  })
  .otherwise({
      redirectTo: "/searching"
  })
})

app.filter('capitalize', function() {
    return function(input) {
      return (angular.isString(input) && input.length > 0) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : input;
    }
});
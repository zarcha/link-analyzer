const { app, BrowserWindow, ipcMain, dialog } = require("electron");
const path = require('path');
const fs = require("fs");
var bmp = require("bmp-js");
const Jimp = require("jimp")
const shortid = require("shortid")
const del = require('del');
const { prependOnceListener } = require("process");

let win,
    debug = false;

let main = {
    createWindow: () => {
        // Create the browser window.
        win = new BrowserWindow({
            width: 700,
            height: 700,
            title: "Link Analyzer",
            frame: false,
            resizable: false,
            webPreferences: {
                nodeIntegration: true
            }
        });

        win.loadFile(path.resolve(__dirname, "web-content/index.html"));
        
        if(debug)
            win.webContents.openDevTools()
    },
    sendMessage: (hookName, arg) => {
        win.webContents.send(hookName, arg);
    },
    sendError: (error) => {
        console.log("Sent error");
        win.webContents.send('errorMessage', error);
    }
}
const comManager = require("./comManager.js")(main);
module.exports = main;
    
app.on("ready", module.exports.createWindow);

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit()
    }
});

app.on('activate', () => {
    if (BrowserWindow.getAllWindows().length === 0) {
        createWindow()
    }
});

ipcMain.on('searchForCom', async (event, arg)=> {
    while(!comManager.isComFound()){
        comPorts = await comManager.getComPorts();
        if(process.platform == "darwin") comPorts = comPorts.filter(value => value.includes("usb"))
        for(let i = 0; i < comPorts.length; i++){
            try{
                console.log(comPorts[i])
                comStatus = await comManager.initialize(comPorts[i]);
                console.log("Com status: " + comStatus);
                event.reply("comFound");
                break;
            }catch(err){
                console.log(err)
            }
        }
    }
});

ipcMain.on('useFakeCom', (event, arg) => {
    comManager.forceComFound();
    event.reply("comFound");
});

ipcMain.on('checkForFake', (event, arg) => {
    event.reply("responseCheckForFake", comManager.isFakeCom());
});

ipcMain.on('sendCommand', async (event, arg) => {
    try{
        comManager.writeCommand(arg);
    }catch(err){
        //localModule.sendError(err)
    }
});

ipcMain.on('sendCommandCallBack', async (event, arg) => {
    try{
        let response = await comManager.writeCommandAndWait(arg);
        event.reply(response);
    }catch(err){
        //localModule.sendError(err)
    }
});

ipcMain.on("detectChip", async (event, arg) => {
    try{
        let response;
        if(!comManager.isFakeCom()){
            response = await comManager.writeCommandAndWait("2");
        }else{
            response = "010001111110"
        }
        
        event.reply("chipPinout", response);
    }catch(err){
        console.log(err);
    }
});

ipcMain.on("getChipLibrary", async (event, arg) => {
    try{
        let response = await getFileData(`${getPath()}/link-chips.json`, "utf8");
        event.reply("responseChipLibrary", JSON.parse(response));
    }catch(err){
        console.log(err)
    }
});

ipcMain.on("getNaviDataLocattions", async (event, arg) => {
    try{
        let response = await getFileData(`${getPath()}/link-data-locations.json`, "utf8");
        event.reply("responseNaviDataLocattions", JSON.parse(response));
    }catch(err){
        console.log(err)
    }
});

ipcMain.on("getNaviData", async (event, arg) => {
    try{
        if(!comManager.isFakeCom()){
            let data = await comManager.writeCommandAndWait("0");
            console.log(data)
            let newData = "";

            for(let i = 0; i < data.length; i+=2){
                newData += int2hex(parseInt(data.substr(i, 2), 16));
            }

            event.reply('responseNaviData', newData);
        }else{
            loadBackup(event, arg)
        }
    }catch(err){
        console.log(err)
    }
});

ipcMain.on("writeNaviData", async (event, arg) => {
    try{
        for(let i = 0; i < 1024; i++){
            let command = `1:${i} ${arg.substr(i * 2, 2)}`;
            
            if(i == 1023){
                await comManager.writeCommandAndWait(command);
            }else{
                console.log(command);
                comManager.writeCommand(command);
            }
        }
        event.reply('cancleWait')
    }catch(err){
        console.log(err)
    }
});

ipcMain.on("getLocation", async (event, arg) => {
    try{
        event.reply("responseLocation", getPath());
    }catch(err){
        console.log(err)
    }
});

ipcMain.on("saveBackup", async (event, arg) => {
    try{

        let file = await dialog.showSaveDialog({defaultPath: getPath() + "/backup.Bin", filters: [{name: "Bin", extensions: ["Bin"]}]})

        if(!file.canceled){

            fs.writeFile(file.filePath, arg, 'HEX', (err) => {
                if(err){
                    console.log(err);
                    return;
                }
        
                console.log("File written")
            })
        }
    }catch(err){
        console.log(err)
    }
});

ipcMain.on("loadBackup", async (event, arg) => {
    try{
        loadBackup(event, arg);
    }catch(err){
        console.log(err)
        localModule.sendError(err)
    }
});

ipcMain.on("loadImage", async (event, arg) => {
    try{
        let file = await dialog.showOpenDialog({defaultPath: getPath() + "/navi.bmp", filters: [{name: "bmp", extensions: ["bmp"]}]})

        if(!file.canceled){
            let data = convertBMPtoHex(file.filePaths[0]);

            if(data == ""){
                main.sendError("Image was not correctly formatted");
                return;
            }

            event.reply('imageChange', {
                data: data,
                name: arg
            });
        }
    }catch(err){
        console.log(err)
    }
});

ipcMain.on("saveImage", async (event, arg) => {
    try{
        console.log("image saved")
        let name = arg.name.split("-")[0]
        console.log(name)
        await del([`./images/${name}*`]);
        convertHextoBMP(arg.name, arg.data);
    }catch(err){
        console.log(err);
    }

});

async function loadBackup(event, arg){
    try{
        let fileName = arg,
            filePath = "",
            file;

        if(!fileName){
            file = await dialog.showOpenDialog({defaultPath: getPath() + "/backup.Bin", filters: [{name: "Bin", extensions: ["Bin"]}]})
            filePath = file.filePaths[0]
        }else{
            filePath = `${getPath()}/base-navi/${fileName}.Bin`
            file = {canceled: false};
        }
        
        if(!file.canceled){
            let data = await getFileData(filePath, 'HEX');
            event.reply('responseNaviData', data);
        }else{
            event.reply('cancleWait');
        }
    }catch(err){
        console.log(err)
        localModule.sendError(err)
    }
}

async function getFileData(file, type){
    return new Promise((resolve, reject) => {
        fs.readFile(file, type, (err, data) => {
            if(err){
                reject(err);
                return;
            }
    
            resolve(data);
        });
    });
}

function hex2int(hex){
    return parseInt(hex, 16);
}

function int2hex(int){
    return ("00" + (int).toString(16)).substr(-2)
}

function hex2bin(hex){
    return ("00000000" + (parseInt(hex, 16)).toString(2)).substr(-8)
}

function bin2hex(bin){
    return ("00" + (parseInt(bin, 2)).toString(16)).substr(-2)
}

function flipEndainess(str){
    return str.split("").reverse().join("")
}

function convertHextoBMP(name, data){
    let height = 32,
        width = height;

    let lines = []
    lines.push("000000000000000000000000000000000")
    for(let j = 0; j < data.length; j+=8){
        let set = data.substr(j, 8);
        let binConverted = "";
        for(let i = 0; i < set.length; i+=2){
            binConverted += flipEndainess(hex2bin(set.substr(i, 2)))
        }

        lines.push(binConverted);
    }
    lines.push("000000000000000000000000000000000")

    let jimg = new Jimp(32, 32);
    for (var y=0; y<height; y++) {
        for(let x = 0; x < width; x++){
            let color = lines[y].substr(x, 1) == "0" ? Jimp.rgbaToInt(255,255,255,1) : Jimp.rgbaToInt(0,0,0,1)
            jimg.setPixelColor(color, x, y);
        }
    }
    jimg.write(`${getPath()}/images/${name}.bmp`)
    jimg.greyscale().rotate(-90).flip(true, false).write(`${getPath()}/images/${name}.jpeg`)   
}

function convertBMPtoHex(file){
    let bmpBuffer = fs.readFileSync(file);
    let decoded = bmp.decode(bmpBuffer);
    if(decoded.height != 32 || decoded.width != 32){
        return "";
    }
    var bmpData = decoded.data.toString('HEX');
    let binResult = "";
    let endResult = "";

    for(let i = 0; i < bmpData.length; i+=8){
        if(bmpData.substr(i, 8) == "00ffffff"){
            binResult += "0"
        }else{
            binResult += "1"
        }
    }

    for(let i = 32; i < binResult.length-32; i+=8){
        endResult += bin2hex(flipEndainess(binResult.substr(i, 8)));
    }
    
    return endResult;
}

function getPath(){
    if(process.platform == "win32" && !debug){
        return `${process.env.PORTABLE_EXECUTABLE_DIR}`;
    }else if(process.platform == "darwin" && !debug){
        return `${app.getPath("documents")}/Link Analyzer`;
    }

    return app.getAppPath()
}

ipcMain.on('closeApp', (event, arg) => {
    process.kill(process.pid);
});


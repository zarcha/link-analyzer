const SerialPort = require('serialport');
const Readline = require('@serialport/parser-readline');
let port;
let parser;
let comFound = false;
let fakeCom = false;
let currentResponse = "";
let kill = false;
let waitingForResponse = false;

module.exports = function(electronManager) {
    let localModule = {
        isComFound: () => {
            return comFound;
        },
        isFakeCom: () => {
            return fakeCom;
        },
        forceComFound: () => {
            comFound = true;
            fakeCom = true
        },
        initialize: async (com) => {
            return new Promise((resolve, reject) => {
                port = new SerialPort(com, (err) => {
                    if(err) reject(err);
                });
                parser = port.pipe(new Readline());

                port.on("open", () => {
                    parser.on('data', async (data) => {
                        if(!comFound){
                            console.log(data)
                            if(data.includes("Command not found")){
                                console.log("found")
                                comFound = true;
                            }
                            return;
                        }

                        currentResponse = data;
                        waitingForResponse = false;
                        
                    })
                
                    setTimeout(async ()=> {

                        setTimeout(()=>{
                            reject("Timeout")
                        }, 10000);

                        returnedRom = await localModule.writeCommand("-1", true);

                        while (!comFound){
                            await __delay__(1000);
                        }
                        resolve("Ready!");
                    }, 2000);
                }); 
            });
        },
        getComPorts: async () => {
            let portPaths = [];
            return new Promise((resolve, reject) => {
                SerialPort.list().then((ports) => {
                    ports.forEach((tempPort) => {
                        portPaths.push(tempPort.path);
                    });

                    resolve(portPaths);
                });
            });
        },
        writeCommandAndWait: async (command) => {
            return new Promise(async (resolve, reject) => {
                try{
                    if(waitingForResponse) kill = true
                    await localModule.writeCommand(command)
                }catch(error){
                    console.log(error)
                    waitingForResponse = false;
                    reject(error);
                    return;
                }
                

                waitingForResponse = true;

                while (waitingForResponse){
                    if(kill) {
                        kill = false;
                        console.log("killed");
                        reject(-2);
                        break;
                    }
                    console.log("waiting")
                    await __delay__(1000);
                }

                resolve(currentResponse);
            });
        },
        writeCommand: async (command) => {
            return new Promise((resolve, reject) => {
                port.write(command + "\n", function(err) {
                    if (err) reject(-1);
                    console.log(command)
                    console.log("Command Sent!");
                    resolve(command);
                });
            });
        }
    }

    function __delay__(timer) {
        return new Promise(resolve => {
            timer = timer || 2000;
            setTimeout(function () {
                resolve();
            }, timer);
        });
    };

    return localModule;
}